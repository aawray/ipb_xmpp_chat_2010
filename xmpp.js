window.xmpp_room = "core@chat.anakee.ru";
window.xmpp_bosh = "/http-bind";
window.xmpp_srv  = "anon.anakee.ru";

var conn = new Strophe.Connection(window.xmpp_bosh ? window.xmpp_bosh : "/bosh");

var nick = window.location.toString().match(/[?&#]nick=([^&#]*)/);

if(nick && nick[1] && nick[1].length > 0)
{
	nick = decodeURI(nick[1]);
}
else
{
	/*nick = prompt("Your name:");*/
	nick = "Гость";
	window.location.hash += "#nick=" + nick;
}

var room = window.location.toString().match(/[?&#]room=([^&#]*)/);
if(room && room[1] && room[1].length > 0)
{
	room = room[1];
	if(room.indexOf('@') < 2)
	{
		room += window.xmpp_room.substring(window.xmpp_room.indexOf('@'));
	}
}
else
{
	room = window.xmpp_room ? window.xmpp_room : "core@conference.anakee.ru";
}

function version_handler(stanza)
{
	conn.send($iq({to: stanza.getAttribute("from"), type: "result", id: stanza.getAttribute("id") })
		.c('query', { xmlns: "jabber:iq:version" })
		.c('name').t(BrowserDetect.browser).up()
		.c('version').t(BrowserDetect.version.toString() + ', Strophe lib fingerprint '+hex_md5(Strophe).substring(0,6) ).up()
		.c('os').t(BrowserDetect.OS).tree());
	return true;
}

function handle_connection_status(status, err)
{
	if(err)
	{
		/* alert(err); */
	}

	if(status == Strophe.Status.CONNECTED)
	{
		conn.addHandler(version_handler, null, "iq", "get", null, null);
		window.muc = create_muc_ui(conn, room, nick,
			{ message_log: document.getElementById('msglog')
				, input_box: document.getElementById('msgfield')
				, occupant_list: document.getElementById('roster')
				, detect_focus: true, tab_completion: true
			}
		);
		var _dis = function () {
			if(!window.conn) return;
			window.conn.send($pres({to: room, type:"unavailable"}).c("status").t("Window closed"));
			window.conn.disconnect();
			window.conn = null;
		};
		addLoader("unload", _dis);
		addLoader("beforeunload", _dis);
				
		document.getElementById('msginput').focus();
		document.getElementById('msginput').select();
	}
}

function show_client()
{
	var obj = null;
	
	window.body = document.getElementById('client');
	
	obj = document.createElement('div');
	obj.setAttribute('id', 'roster');
	window.body.appendChild(obj);
	
	obj = document.createElement('div');
	obj.setAttribute('id', 'msglog');
	window.body.appendChild(obj);
	
	obj = document.createElement('div');
	obj.setAttribute('id', 'msgaction');
	window.body.appendChild(obj);
	
	obj = document.createElement('div');
	obj.setAttribute('id', 'msginput');
	window.body.appendChild(obj);
	
	obj = document.createElement('span');
	obj.setAttribute('class', 'text');
	obj.innerHTML = 'xmpp:' + room;
	document.getElementById('msgaction').appendChild(obj);
	
	obj = document.createElement('span');
	obj.setAttribute('id', 'clear_button');
	obj.setAttribute('class', 'button');
	obj.innerHTML = 'Clear';
	obj.onclick = function() {
		$("#msglog div").fadeOut(500,
			function() { $("#msglog div").remove(); }
		);
	};
	document.getElementById('msgaction').appendChild(obj);
	
	obj = document.createElement('span');
	obj.setAttribute('id', 'send_button');
	obj.setAttribute('class', 'button');
	obj.onclick = function() {
		var input_box = document.getElementById('msgfield');
		var msg = input_box.value;
		if(msg.charAt(0)!="/" || msg.match(action_pattern))
		{
			muc.send_message();
			input_box.value = '';
		}
		else if (msg.substring(0,6)=='/clear')
		{
			$("#msglog div").fadeOut(500,
				function() { $("#msglog div").remove(); }
			);
		}
		else if (msg.substring(0,5)=='/name')
		{
			
		}
		else if (msg.substring(0,5)=='/quit')
		{
			conn.disconnect();
			input_box.value = '';
		}
		return false;
	};
	obj.innerHTML = 'Send';
	document.getElementById('msgaction').appendChild(obj);
	
	/*
	obj = document.createElement('button');
	obj.innerHTML = 'Exit';
	obj.onClick = function() { window.close(); };
	document.getElementById('msgaction').appendChild(obj);
	*/
	
	obj = document.createElement('textarea');
	obj.setAttribute('id', 'msgfield');
	/* obj.onfocus = function() { this.value = this.value; } */
	document.getElementById('msginput').appendChild(obj);
}

start_connection = function () {
	if(nick.length == 0) nick = "itriedtologinwithnonick";
	/*if(nick.length > 24) nick = nick.substr(0, 20);*/
	show_client();
	conn.connect((window.xmpp_srv ? window.xmpp_srv : "anon.anakee.ru"), null, handle_connection_status, 50);
}

replyTo = function (str) {
	pol = str+', ';
	fld = document.getElementById('msgfield');
	if (fld.value.indexOf(pol) < 0) {
		fld.value = pol+fld.value;
	}
	fld.focus();
	return false;
}
